This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gCube Catalogue (gCat) Service

## [v2.6.0]

- Enhanced gcube-smartgears-bom version to 2.5.1 [#27999]


## [v2.5.4]

- Added Content-Location HTTP Header in the response of Item creation #27940


## [v2.5.3] 

- Improved profile schema #26471
- Catalogue core operation has been moved in a dedicated library #27118


## [v2.5.2] 

- Enforce private to a rejected item to avoid issue #26391
- Added profile validation (i.e. regex validity and default value conformity) #26142


## [v2.5.1]

- Migrated code to reorganized E/R format [#24992]
- Moderation notification are now sent as separated thread [#25614]


## [v2.5.0]

- Switched from commons-lang3 to commons-lang to avoid duplicates
- Set resource-registry-publisher dependency scope to provided
- Fixed RequestFilter to avoid to remove info to Smartgears
- Switching to Facet Based IS [#20225]
- Delete of item in a moderated catalogue produces a notification [#24305]
- The user performing create/update item in moderated catalogue receive confirmation via notification of the action [#23575]
- Enhanced gcube-smartgears-bom version
- Added support for JSON:API on 'licenses' collection [#24601]


## [v2.4.1] [r5.14.0] - 2022-12-07

- Integrating Sphinx for documentation [#23833]
- Migrated Social service interaction to social-service-client [#23151]
- Improved author rewrite in case of update [#23851]
- Fixed item listing [#23901]


## [v2.4.0] [r5.13.1] - 2022-09-16

- Added moderation link in moderation message [#23142]
- Added query parameter in item listing to get the whole item instead of just the name [#23691]
- Using renamed constant from gcat-api
- Explict request for approved items return only moderated and approved [#23696]


## [v2.3.0] [r5.13.0] - 2022-07-22

- Switched moderation messages to notification [#23317]
- Item listing returns items in the default organization and not in all supported organization


## [v2.2.0] [r5.11.0] - 2022-05-12

- Switched gcat credentials to new IAM authz [#21628][#22727]
- Added support to manage configurations [#22658][#22742]
- Migrated service to SecretManagerProvider [#22871]
- Migrated to ServiceClass corresponding to Maven groupId
- Added Enunciate to automatically create REST APIs documentation [#23096]
- Fixed 'offset' parameter behaviuor in item listing [#22999]
- Moderation message are sent using gcube messaging system via Social Service [#23117]
- Remove enforcement on approved item for Catalogue-Editor added enforcement to email [#23154]
- ClientID requests are now properly supported [#21903] 


## [v2.1.0] [r5.7.0] - 2022-01-27

- Added query parameter social_post_notification to override default VRE behaviour [#21345]
- Users are created/referenced in the form <Surname Name> and not vice-versa [#21479]
- Added support for moderation [#21342]
- Added support for IAM authz [#21628] 
- Added items bulk delete/purge [#22299]
- Using UriResolverManager to get item URL in place of direct HTTP call [#22549]
- Added empty trash API [#13322]


## [v2.0.0] [r5.2.0] - 2021-05-04

- Fixed retrieving of filename from content-disposition http header used to persist a resource [#21216]
- Fixed author and maintainer name and email [#21059][#21189]
- Improved check on controlled vocabulary to match corner cases [#20742]
- Added PATCH method on Item collection [#19768]
- Switched JSON management to gcube-jackson [#19735]
- Added support to publish an item organizations not matching the current context [#19365]


## [v1.4.5] [r5.1.0] - 2021-03-31

- Removed 'owner_org' field from result when reading an item [#20919]
- Dirty patched item validator [#20965]
- Improved error message return with message got from CKAN [#19516]


## [v1.4.4] [r5.0.0] - 2021-02-24

- Added count method for Item collection [#20627]
- Added count method for Organization, Group and Profile collection [#20629]
- Switched JSON management to gcube-jackson [#19735]


## [v1.4.3] [r4.23.0] - 2020-06-16

- Social Post is disabled if not explicitly enabled by the client [#19295] 
- Solved null pointer exception while creating the JSON object to send to CKAN to create the user [#19395]


## [v1.4.2] [r4.20.0] - 2020-02-14

- Added method to retrieve a persisted file
- Considering that a file could not be persisted because it could be published via portlet


## [v1.4.1] [r4.18.0] - 2019-12-20

- Fixed distro files and pom according to new release procedure
- Fixed metadata checks


## [v1.4.0] [r4.15.0] - 2019-11-06

- Items listing API when invoked at VO level allow to get all the catalogue items or filtering them by organization  [#17635]
- Enabled the possibility of filtering the result of items listing API [#17645]
- User information are checked/updated on CKAN using portal information [#16360]


## [v1.3.0] [r4.14.0] - 2019-06-12

- Removed the requirement of providing the id of an item in update [#16740]
- Resource URL existence is not checked anymore [#16748]


## [v1.2.0-SNAPSHOT] - 2019-05-20 

- Separated REST class for Profile management from the logic which effectively manage profile on IS
- Tags are now properly created/added according to profile definition [#16182]
- Groups are now properly created/added according to profile definition [#16183]
- Setting format in resources [#16347]
- Validated provided LicenseId for Item [#16354]
- Capacity field is removed from the provided item content if any [#16410]
- Fixed bug on getting responses from social service [#16330]
- Added the possibility to disable social post on item creation [#13335]
- Fixed bug on social post creation [#16322]
- Improved social post message [#16322] 
- Added support to publish in specific organizations when the request is not performed at VRE level [#16635]


## [v1.1.0] [r4.13.1] - 2019-02-26

- Added Item URL via URI Resolver in extras field [#13309]
- The final URL is retrieved only URI resolver pointing to Storage Hub [#13303]
- Enforced items to be searchable in 'extras' field [#13306]
- Switched item listing to use package_search in place of package_list [#13307]
- Refactored to use storage-hub-persistence library [#13076] 
- Added Create/Update/Delete in Profile Collection [#13079] 
- Fixed issues in Resource collection
- Refactored to use gcat-api library


## [v1.0.0-SNAPSHOT] - 2019-01-10

- First Version

