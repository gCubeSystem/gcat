
                                                                                  Configuration
                                                                           ---------------------------
                                                          IsCustomizedBy   |                         |
                                                        -----------------> | catalogue-configuration |
                                                       /                   |                         |
                                                      /                    ---------------------------
     EService                        VirtualService  /
   ------------               -----------------------------
   |          |   CallsFor    |                           |
   |   gcat   | ------------> | catalogue-virtual-service |
   |          |               |                           |
   ------------               -----------------------------
                                                    \                                                                 EService
                                                     \                                                          --------------------
                                                      \                                             Uses        |                  |                         
                                                       \                                    ------------------> | postgres-ckan-db |
                                                        \                                  /                    |                  |
                                                         \                     EService   /                     --------------------
                                                          \                -----------------
                                                           \   CallsFor    |               |
                                                            -------------> |     ckan      |
                                                                           |               |
                                                                           -----------------                          EService   
                                                                                          \                     --------------------
                                                                                           \         Uses       |                  |
                                                                                            ------------------> |       solr       |
                                                                                                                |                  |
                                                                                                                --------------------
                                                                                                                      