package org.gcube.gcat;

import java.io.IOException;

import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.gcat.configuration.CatalogueConfigurationFactory;
import org.gcube.gcat.configuration.isproxies.impl.FacetBasedISConfigurationProxy;
import org.gcube.gcat.configuration.isproxies.impl.FacetBasedISConfigurationProxyFactory;
import org.gcube.gcat.configuration.isproxies.impl.GCoreISConfigurationProxyFactory;
import org.gcube.gcat.rest.RequestFilter;
import org.gcube.smartgears.ApplicationManager;
import org.gcube.smartgears.ContextProvider;
import org.gcube.smartgears.context.application.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GCatInitializator implements ApplicationManager {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(GCatInitializator.class);
	
	public static boolean initialised;
	
	/** 
	 * {@inheritDoc}
	 * The method discover the plugins available on classpath and their own 
	 * supported capabilities and publish a ServiceEndpoint with the 
	 * discovered information.
	 * Furthermore create/connect to DB
	 */
	@Override
	public synchronized void onInit() {
		RequestFilter requestFilter = new RequestFilter();
		try {
			requestFilter.filter(null);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		String context = SecretManagerProvider.instance.get().getContext();
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "GCat is Starting on context {}\n"
				+ "-------------------------------------------------------",
				context);
		
		ApplicationContext applicationContext = ContextProvider.get();
		String gcatEServiceID  = applicationContext.id();
		
		
		try {
			FacetBasedISConfigurationProxyFactory fbigcpf = new FacetBasedISConfigurationProxyFactory();
			FacetBasedISConfigurationProxy facetBasedISConfigurationProxy = fbigcpf.getInstance(context);
			if(!initialised) {
				CatalogueConfigurationFactory.addISConfigurationProxyFactory(fbigcpf);
				GCoreISConfigurationProxyFactory gcigcpf = new GCoreISConfigurationProxyFactory();
				CatalogueConfigurationFactory.addISConfigurationProxyFactory(gcigcpf);
				initialised = true;
				facetBasedISConfigurationProxy.installQueryTemplate();
			}
			facetBasedISConfigurationProxy.setServiceEServiceID(gcatEServiceID);
			facetBasedISConfigurationProxy.createCallsForToVirtualService();
		}catch (Exception e) {
			logger.warn("Gcat is not configured through the Facet Based IS in context {}. Please create/addToContext the expected resources ASAP. The Gcore IS will be used.", context);
			logger.trace("Gcat is not configured through the Facet Based IS in context {}. The reason is:\n", context, e);
		}
		
		
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "GCat Started Successfully on context {}\n"
				+ "-------------------------------------------------------",
				context);
		
	}
	
	/** 
	 * {@inheritDoc} 
	 * This function is invoked before the service will stop and unpublish the 
	 * resource from the IS to maintain the infrastructure integrity.
	 * Furthermore close the connection to DB.
	 */
	@Override
	public synchronized void onShutdown(){
		RequestFilter requestFilter = new RequestFilter();
		try {
			requestFilter.filter(null);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		String context = SecretManagerProvider.instance.get().getContext();
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "GCat is Stopping on context {}\n"
				+ "-------------------------------------------------------", 
				context);
		
		/*
		 * We don't delete the relation.
		 * Thanks to the propagation constraint, 
		 * it will be deleted when the EService will be deleted.
		 *  
		ApplicationContext applicationContext = ContextProvider.get();
		String gcatEServiceID  = applicationContext.id();
		
		FacetBasedISGcatConfigurationProxyFactory fbigcpf = new FacetBasedISGcatConfigurationProxyFactory();
		FacetBasedGcatISConfigurationProxy facetBasedISConfigurationProxy = fbigcpf.getInstance(context);
		facetBasedISConfigurationProxy.setGcatEServiceID(gcatEServiceID);
		try {
			facetBasedISConfigurationProxy.deleteCallsForToVirtualService();
		}catch (Exception e) {
			logger.warn("Gcat is not configured through the Facet Based IS in context {}. Please create/addToContext the expected resources ASAP. The Gcore IS will be used.", context, e);
		}
		*/
		
		logger.trace(
				"\n-------------------------------------------------------\n"
				+ "GCat Stopped Successfully on context {}\n"
				+ "-------------------------------------------------------", 
				context);
	}
}
