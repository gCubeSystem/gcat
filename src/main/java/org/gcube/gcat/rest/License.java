package org.gcube.gcat.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.WebServiceException;

import org.gcube.gcat.api.GCatConstants;
import org.gcube.gcat.persistence.ckan.CKANLicense;

import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;

/**
 * A collection to interact with licenses that are available in the catalogue.
 * A license is associated with items to define the legal right
 * to use such an item.
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(License.LICENSES)
@ResourceGroup("Item Related APIs")
@ResourceLabel("License APIs")
public class License extends REST<CKANLicense> implements org.gcube.gcat.api.interfaces.License {
	
	public License() {
		super(LICENSES, null, CKANLicense.class);
	}
	
	/**
	 * @return the json array containing the licenses available in the catalogue
	 * 
	 * @pathExample /licenses
	 * @responseExample application/json classpath:/api-docs-examples/license/list-license-response.json
	 */
	@GET
	@Produces({MediaType.APPLICATION_JSON,GCatConstants.APPLICATION_JSON_API})
	public String list() throws WebServiceException {
		String ret = super.list(-1, -1);
		
		String accept = httpHeaders.getHeaderString("Accept");
		if(accept!=null && accept.contains(GCatConstants.APPLICATION_JSON_API)) {
			return resultAsJsonAPI(ret);
		}
		return ret;
	}
	
}
