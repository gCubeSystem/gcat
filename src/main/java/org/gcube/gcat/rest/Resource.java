package org.gcube.gcat.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
//import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

//import org.gcube.common.authorization.control.annotations.AuthorizationControl;
import org.gcube.gcat.annotation.PATCH;
import org.gcube.gcat.api.GCatConstants;
//import org.gcube.gcat.api.roles.Role;
import org.gcube.gcat.persistence.ckan.CKANResource;

import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;

/**
 * An Item's Resource is an URL (e.g. URL pointing to a file)
 * and can exists only attached to an <a href="./resource_Item.html">item</a>.
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(Resource.COLLECTION)
@ResourceGroup("Item APIs")
@ResourceLabel("Item's Resource APIs")
public class Resource extends BaseREST implements org.gcube.gcat.api.interfaces.Resource<Response,Response> {
	
	protected static final String ITEM_ID_PARAMETER = Item.ITEM_ID_PARAMETER;
	protected static final String RESOURCE_ID_PARAMETER = "RESOURCE_ID";
	
	protected static final String COLLECTION = Item.ITEMS + "/{" + ITEM_ID_PARAMETER + "}/" + RESOURCES;
	
	@GET
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	/* Catalogue-Member is not added to VRE members and is assumed as the default role in the catalogue for the VRE members. So we can't enforce
	 * @AuthorizationControl(allowedRoles={Role.CATALOGUE_MEMBER, Role.CATALOGUE_EDITOR, Role.CATALOGUE_ADMIN, Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	 */
	public String list(@PathParam(ITEM_ID_PARAMETER) String itemID) {
		setCalledMethod("GET /" + COLLECTION);
		CKANResource ckanResource = new CKANResource(itemID);
		ckanResource.setName(itemID);
		return ckanResource.list();
	}
	
	/**
	 * An Resource is mainly described by the following attributes (* indicate mandatory attributes):
	 * 
	 * <dl>
	 * 	<dt>name* (string)</dt>
	 * 	<dd>the name of the resource;</dd>
	 * 
	 * 	<dt style="margin-top: 5px;">url* (string)</dt>
	 * 	<dd>url of resource;</dd>
	 * 
	 * 	<dt style="margin-top: 5px;">package_id* (string)</dt>
	 * 	<dd>id of the item that the resource should be added to;</dd>
	 * 
	 * 	<dt style="margin-top: 5px;">revision_id (string)</dt>
	 * 	<dd></dd>
	 * 
	 *	<dt style="margin-top: 5px;">description (string)</dt>
	 * 	<dd>a description for the resource;</dd> 
	 * 
	 * 	<dt style="margin-top: 5px;">format (string)</dt>
	 * 	<dd></dd>
	 * 
	 *  <dt style="margin-top: 5px;">mimetype (string)</dt>
	 * 	<dd>resource mimetype;</dd>
	 * 
	 * 	<dt style="margin-top: 5px;">created (iso date string)</dt>
	 * 	<dd>resource creation time;</dd>
	 * 
	 * 	<dt style="margin-top: 5px;">last_modified (iso date string)</dt>
	 * 	<dd>resource last update time;</dd>
	 * 
	 * </dl>
	 */
	@POST
	@Consumes(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_EDITOR, Role.CATALOGUE_ADMIN, Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public Response create(@PathParam(ITEM_ID_PARAMETER) String itemID, String json) {
		setCalledMethod("POST /" + COLLECTION);
		CKANResource ckanResource = new CKANResource(itemID);
		String ret = ckanResource.create(json);
		
		ResponseBuilder responseBuilder = Response.status(Status.CREATED).entity(ret);
		responseBuilder = addLocation(responseBuilder, ckanResource.getResourceID());
		return responseBuilder.type(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8).build();
	}
	
	@GET
	@Path("/{" + RESOURCE_ID_PARAMETER + "}")
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	/* Catalogue-Member is not added to VRE members and is assumed as the default role in the catalogue for the VRE members. So we can't enforce
	 * @AuthorizationControl(allowedRoles={Role.CATALOGUE_MEMBER, Role.CATALOGUE_EDITOR, Role.CATALOGUE_ADMIN, Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	 */
	public String read(@PathParam(ITEM_ID_PARAMETER) String itemID,
			@PathParam(RESOURCE_ID_PARAMETER) String resourceID) {
		setCalledMethod("GET /" + COLLECTION + "/{" + RESOURCE_ID_PARAMETER + "}");
		CKANResource ckanResource = new CKANResource(itemID);
		ckanResource.setResourceID(resourceID);
		return ckanResource.read();
	}
	
	@PUT
	@Path("/{" + RESOURCE_ID_PARAMETER + "}")
	@Consumes(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_EDITOR, Role.CATALOGUE_ADMIN, Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public String update(@PathParam(ITEM_ID_PARAMETER) String itemID,
			@PathParam(RESOURCE_ID_PARAMETER) String resourceID, String json) {
		setCalledMethod("PUT /" + COLLECTION + "/{" + RESOURCE_ID_PARAMETER + "}");
		CKANResource ckanResource = new CKANResource(itemID);
		ckanResource.setResourceID(resourceID);
		return ckanResource.update(json);
	}
	
	@PATCH
	@Path("/{" + RESOURCE_ID_PARAMETER + "}")
	@Consumes(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_EDITOR, Role.CATALOGUE_ADMIN, Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public String patch(@PathParam(ITEM_ID_PARAMETER) String itemID,
			@PathParam(RESOURCE_ID_PARAMETER) String resourceID, String json) {
		setCalledMethod("PATCH /" + COLLECTION + "/{" + RESOURCE_ID_PARAMETER + "}");
		CKANResource ckanResource = new CKANResource(itemID);
		ckanResource.setResourceID(resourceID);
		return ckanResource.patch(json);
	}
	
	@DELETE
	@Path("/{" + RESOURCE_ID_PARAMETER + "}")
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_EDITOR, Role.CATALOGUE_ADMIN, Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public Response delete(@PathParam(ITEM_ID_PARAMETER) String itemID,
			@PathParam(RESOURCE_ID_PARAMETER) String resourceID) {
		setCalledMethod("DELETE /" + COLLECTION + "/{" + RESOURCE_ID_PARAMETER + "}");
		CKANResource ckanResource = new CKANResource(itemID);
		ckanResource.setResourceID(resourceID);
		ckanResource.delete(false);
		return Response.status(Status.NO_CONTENT).build();
	}
	
}
