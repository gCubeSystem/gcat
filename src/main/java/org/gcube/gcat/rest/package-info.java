/**
 * <h1>gCube Catalogue (gCat) Service</h1>
 * 
 * <p>Welcome to gCube Catalogue Service (aka gCat) API documentation.</p>
 * 
 * <p>
 * 	To get a complete overview of gCat service take a look at 
 * 	<a href="../docs/index.html">wiki page</a>.
 * </p>
 * 
 * 
 */
package org.gcube.gcat.rest;