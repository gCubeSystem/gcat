package org.gcube.gcat.rest;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;

import org.gcube.common.authorization.library.provider.CalledMethodProvider;
import org.gcube.gcat.api.GCatConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webcohesion.enunciate.metadata.rs.RequestHeader;
import com.webcohesion.enunciate.metadata.rs.RequestHeaders;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@RequestHeaders ({
	  @RequestHeader( name = "Authorization", description = "Bearer token, see <a href=\"https://dev.d4science.org/how-to-access-resources\">https://dev.d4science.org/how-to-access-resources</a>")
})
public class BaseREST {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Context
	protected HttpHeaders httpHeaders;
	
	@Context
	protected UriInfo uriInfo;
	
	protected static final String LOCATION_HEADER = "Location";
	
	protected void setCalledMethod(String method) {
		CalledMethodProvider.instance.set(method);
		logger.info("{}", uriInfo.getAbsolutePath());
	}
	
	protected ResponseBuilder addLocation(ResponseBuilder responseBuilder, String id) {
		return responseBuilder.header(LOCATION_HEADER,
				String.format("%s/%s", uriInfo.getAbsolutePath().toString(), id));
	}
	
	protected String createCountJson(int count) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("{\"");
		stringBuilder.append(GCatConstants.COUNT_KEY);
		stringBuilder.append("\":");
		stringBuilder.append(count);
		stringBuilder.append("}");
		return stringBuilder.toString();
	}
	
}
