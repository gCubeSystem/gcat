package org.gcube.gcat.rest;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
//import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.ws.WebServiceException;

//import org.gcube.common.authorization.control.annotations.AuthorizationControl;
import org.gcube.gcat.annotation.PURGE;
import org.gcube.gcat.api.GCatConstants;
//import org.gcube.gcat.api.roles.Role;
import org.gcube.gcat.persistence.ckan.CKANPackageTrash;

import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;
import com.webcohesion.enunciate.metadata.rs.ResponseCode;
import com.webcohesion.enunciate.metadata.rs.StatusCodes;

/**
 * This collection allow to interact with thrashed items
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(Trash.TRASH)
@ResourceGroup("Item Related APIs")
@ResourceLabel("Trash APIs")
public class Trash extends BaseREST implements org.gcube.gcat.api.interfaces.Trash<Response> {

	
	/**
	 * List the thrashed items.<br/>
	 * By default, it lists only the trashed items of the requesting user.<br/>
	 * 
	 * The listed items belong to the supported organizations for the 
	 * context of the request (i.e. the context where the token has been generated).<br/>
	 *  
	 * See <a href="./resource_Configuration.html">supportedOrganizations parameter in
	 * Configuration</a>.
	 * 
	 * If the user specifies <code>own_only=false</code> and the user is
	 * <em>Catalogue-Admin</em> or above it return the thrashed items of all the 
	 * users for all the supported organizations. 
	 * 
	 * @param ownOnly indicates that the user is interested only in its own items or the items of all the users.
	 * @return the json array containing the items in the trash
	 * 
	 * @pathExample /trash
	 * @responseExample application/json ["item1","item54"]
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Override
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_EDITOR, Role.CATALOGUE_ADMIN, Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public String list(@QueryParam(GCatConstants.OWN_ONLY_QUERY_PARAMETER) @DefaultValue("true") Boolean ownOnly) throws WebServiceException {
		CKANPackageTrash ckanPackageTrash = new CKANPackageTrash();
		ckanPackageTrash.setOwnOnly(ownOnly);
		return ckanPackageTrash.list();
	}
	
	/**
	 * Delete in background all items in the trash.<br/>
	 * The operation returns immediately to the client and continues in background.<br/>
	 * There is no way to monitor or stop the running operation.<br/>
	 * The thrashed items cannot be recovered.</br/>
	 * 
	 * By default, it empty only the trashed items of the requesting user.<br/>
	 * 
	 * The listed items belong to the supported organizations for the 
	 * context of the request (i.e. the context where the token has been generated).<br/>
	 *  
	 * See <a href="./resource_Configuration.html">supportedOrganizations parameter in
	 * Configuration</a>.
	 * 
	 * If the user specifies <code>own_only=false</code> and the user is
	 * <em>Catalogue-Admin</em> or above it empty the thrash of all the 
	 * users for all the supported organizations. 
	 * 
	 * @param ownOnly indicates that the user is interested only in its own items or the items of all the users.
	 * @return 202 Accepted if the request has been accepted successfully
	 * @throws WebServiceException
	 * 
	 * @pathExample /trash
	 */
	@DELETE
	@StatusCodes ({
		@ResponseCode ( code = 202, condition = "The empty trash operation has been accepted successfully.")
	})
	@Override
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_EDITOR, Role.CATALOGUE_ADMIN, Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public Response empty(@QueryParam(GCatConstants.OWN_ONLY_QUERY_PARAMETER) @DefaultValue("true") Boolean ownOnly) throws WebServiceException {
		Thread thread = new Thread(new Runnable() {
		    @Override
		    public void run() {
		    	CKANPackageTrash ckanPackageTrash = new CKANPackageTrash();
		    	ckanPackageTrash.setOwnOnly(ownOnly);
		    	ckanPackageTrash.empty();
		    }
		});  
		thread.start();
		return Response.status(Status.ACCEPTED).build(); 
	}
	
	/**
	 * It is the same of 
	 * <a href="resource_Trash.html#resource_Trash_empty_ownOnly_DELETE">DELETE</a> operation
	 */
	@PURGE
	@StatusCodes ({
		@ResponseCode ( code = 202, condition = "The empty trash operation has been accepted successfully.")
	})
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_EDITOR, Role.CATALOGUE_ADMIN, Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public Response emptyViaPurge(@QueryParam(GCatConstants.OWN_ONLY_QUERY_PARAMETER) @DefaultValue("true") Boolean ownOnly) throws WebServiceException {
		return empty(ownOnly); 
	}
	
}