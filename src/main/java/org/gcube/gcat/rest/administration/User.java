package org.gcube.gcat.rest.administration;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
//import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.gcat.annotation.PURGE;
//import org.gcube.common.authorization.control.annotations.AuthorizationControl;
import org.gcube.gcat.api.GCatConstants;
import org.gcube.gcat.api.roles.Role;
//import org.gcube.gcat.api.roles.Role;
import org.gcube.gcat.persistence.ckan.CKANUser;
import org.gcube.gcat.persistence.ckan.cache.CKANUserCache;
import org.gcube.gcat.rest.REST;

import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;

/**
 * This collection allows to interact with the catalogue users.
 * 
 * Only Catalogue-Admins or above are able to invoke non-safe methods.
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(User.USERS)
@ResourceGroup("Administration APIs")
@ResourceLabel("User APIs")
public class User extends REST<CKANUser> implements org.gcube.gcat.api.interfaces.User<Response,Response> {
	
	protected static final String GCUBE_USERNAME_PARAMETER = "GCUBE_USERNAME";
	protected static final String CKAN_USER_ID_PARAMETER = "USER_ID";
	
	public User() {
		super(USERS, CKAN_USER_ID_PARAMETER, CKANUser.class);
	}
	
	/**
	 * @pathExample /users
	 * @responseExample application/json;charset=UTF-8 classpath:/api-docs-examples/user/list-user-response.json
	 */
	@GET
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public String list() {
		return super.list(-1, -1);
	}
	
	/**
	 * A User is mainly described by the following attributes (* indicate mandatory attributes):
	 * 
	 * <dl>
	 * 	
	 * 	<dt>name* (string)</dt>
	 * 	<dd>
	 * 		the name of the user, a string between 2 and 100 characters long, 
	 * 		containing only lowercase alphanumeric characters, '-' and '_' ;
	 * 	</dd>
	 *	
	 *	<dt style="margin-top: 5px;">id (string)</dt>
	 *  <dd>the id of the user;</dd>
	 *  
	 *	<dt style="margin-top: 5px;">fullname (string)</dt>
	 *  <dd>the fullname of the user;</dd>
	 *  
	 *	<dt style="margin-top: 5px;">email (string)</dt>
	 *  <dd>the email address for the user;</dd>
	 *  
	 *	<dt style="margin-top: 5px;">password (string)</dt>
	 *  <dd>
	 *  	the password of the user, a string of at least 4 characters 
	 *  	(parameter only used internally to create a new user in Ckan);
	 *  </dd>
	 * 	
	 * 	<dt style="margin-top: 5px;">about (string)</dt>
	 *  <dd>a description of the user.</dd>
	 *  
	 * </dl>
	 * 
	 */
	@POST
	@Consumes(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Override
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public Response create(String json) {
		return super.create(json);
	}
	
	/**
	 * Read the profile of the user in Ckan
	 * The <code>apikey</code> to interact directly with Ckan are returned only for Catalogue-Managers
	 * and only for its own profile.
	 * 
	 * The service add the user's role in <code>portal_role</code> property when the user read its own profile.
	 * See <a href="../docs/index.html#roles">Roles</a> section for more information.
	 * 
	 * @param username Ckan or gCube username are both accepted
	 * 
	 * @pathExample /users/luca.frosini
	 * @responseExample application/json;charset=UTF-8 classpath:/api-docs-examples/user/read-user-response.json
	 */
	@GET
	@Path("/{" + CKAN_USER_ID_PARAMETER + "}")
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Override
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public String read(@PathParam(CKAN_USER_ID_PARAMETER) String username) {
		return super.read(username);
	}
	
	@PUT
	@Path("/{" + CKAN_USER_ID_PARAMETER + "}")
	@Consumes(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Override
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public String update(@PathParam(CKAN_USER_ID_PARAMETER) String username, String json) {
		return super.update(username, json);
	}
	
	@PURGE
	@Path("/{" + CKAN_USER_ID_PARAMETER + "}")
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public Response purge(@PathParam(CKAN_USER_ID_PARAMETER) String username) {
		return super.delete(username, true);
	}
	
	
	@DELETE	
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public Response removeCallerFromCache() {
		// Any user can remove self from cache
		CKANUserCache.removeUserFromCache();
		return Response.status(Status.NO_CONTENT).build(); 
	}
	
	@DELETE
	@Path("/{" + GCUBE_USERNAME_PARAMETER + "}")
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public Response removeUserFromCache(@PathParam(GCUBE_USERNAME_PARAMETER) String username) {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		org.gcube.common.authorization.utils.user.User user = secretManager.getUser();
		if(user.getRoles().contains(Role.MANAGER.getPortalRole()) || user.getUsername().compareTo(username)==0) {
			// Any user can remove self from cache
			CKANUserCache.removeUserFromCache(username);
		}else {
			throw new ForbiddenException("Only " + Role.MANAGER.getPortalRole() + "s are authorized to remove an user from the cache");
		}
		return Response.status(Status.NO_CONTENT).build(); 
	}
	
}
