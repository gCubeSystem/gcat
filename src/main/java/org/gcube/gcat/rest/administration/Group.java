package org.gcube.gcat.rest.administration;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
//import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.xml.ws.WebServiceException;

//import org.gcube.common.authorization.control.annotations.AuthorizationControl;
import org.gcube.gcat.annotation.PATCH;
import org.gcube.gcat.annotation.PURGE;
import org.gcube.gcat.api.GCatConstants;
//import org.gcube.gcat.api.roles.Role;
import org.gcube.gcat.persistence.ckan.CKANGroup;
import org.gcube.gcat.rest.REST;

import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;

/**
 * This concept is mutated by Ckan which is used as underling technology to persist items.
 * 
 * Only Catalogue-Admins or above are able to invoke non-safe methods.
 * 
 * @author Luca Frosini (ISTI - CNR)
 * 
 */
@Path(Group.GROUPS)
@ResourceGroup("Administration APIs")
@ResourceLabel("Group APIs")
public class Group extends REST<CKANGroup> implements org.gcube.gcat.api.interfaces.Group<Response,Response> {
	
	protected static final String GROUP_ID_PARAMETER = "GROUP_ID";
	
	public Group() {
		super(GROUPS, GROUP_ID_PARAMETER, CKANGroup.class);
	}
	
	/*
	 * Not used as REST method, implemented to respect {@link org.gcube.gcat.api.interfaces.Item} interface
	 */
	@Override
	public int count() throws WebServiceException {
		CKANGroup ckan = getInstance();
		return ckan.count();
	}
	
	@GET
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_EDITOR, Role.CATALOGUE_ADMIN, Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public String list(@QueryParam(GCatConstants.LIMIT_QUERY_PARAMETER) @DefaultValue("10") int limit,
			@QueryParam(GCatConstants.OFFSET_QUERY_PARAMETER) @DefaultValue("0") int offset, 
			@QueryParam(GCatConstants.COUNT_QUERY_PARAMETER) @DefaultValue("false") Boolean countOnly) {
		if(countOnly) {
			int count = count();
			return createCountJson(count);
		}else {
			return list(limit, offset);
		}
	}
	
	/*
	 * Not used as REST method, implemented to respect {@link org.gcube.gcat.api.interfaces.Item} interface
	 */
	@Override
	public String list(@QueryParam(GCatConstants.LIMIT_QUERY_PARAMETER) @DefaultValue("10") int limit,
			@QueryParam(GCatConstants.OFFSET_QUERY_PARAMETER) @DefaultValue("0") int offset) {
		return super.list(limit, offset);
	}
	
	/**
	 * 
	 * 
	 * A group is mainly described by the following attributes (* indicate mandatory attributes):
	 * 
	 * <dl>
	 * 	
	 * 	<dt>name* (string)</dt>
	 * 	<dd>
	 * 		the name of the group, a string between 2 and 100 characters long, 
	 * 		containing only lowercase alphanumeric characters, '-' and '_' ;
	 * 	</dd>
	 *	
	 *	<dt style="margin-top: 5px;">id (string)</dt>
	 *  <dd>the id of the group;</dd>
	 *  
	 *	<dt style="margin-top: 5px;">title (string)</dt>
	 *  <dd>the title of the group;</dd>
	 *  
	 *	<dt style="margin-top: 5px;">description (string)</dt>
	 *  <dd>the description of the group;</dd>
	 *  
	 *	<dt style="margin-top: 5px;">image_url (string)</dt>
	 *  <dd>the URL to an image to be displayed on the group’s page;</dd>
	 * 	
	 * 	<dt>state (string, default: 'active')</dt>
	 * 	<dd>
	 * 		the current state of the group, e.g. 'active' or 'deleted', 
	 * 		only active groups show up in search results and other lists of groups, 
	 * 		this parameter will be ignored if you are not authorized 
	 * 		to change the state of the group;
	 * 	</dd>
	 *  
	 *  <dt style="margin-top: 5px;">extras (list of dataset extra dictionaries)</dt>
	 *  <dd>
	 *  	the group’s extras, extras are arbitrary (key: value) metadata that can be added to groups, 
	 *  	each extra dictionary should have keys 'key' (a string), 
	 *  	'value' (a string), and optionally 'deleted'.
	 *  </dd>
	 *  
	 * </dl>
	 * 
	 */
	@POST
	@Consumes(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Override
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public Response create(String json) {
		return super.create(json);
	}
	
	@GET
	@Path("/{" + GROUP_ID_PARAMETER + "}")
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Override
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_EDITOR}, exception=NotAuthorizedException.class)
	public String read(@PathParam(GROUP_ID_PARAMETER) String id) {
		return super.read(id);
	}
	
	@PUT
	@Path("/{" + GROUP_ID_PARAMETER + "}")
	@Consumes(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Override
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public String update(@PathParam(GROUP_ID_PARAMETER) String id, String json) {
		return super.update(id, json);
	}
	
	@PATCH
	@Path("/{" + GROUP_ID_PARAMETER + "}")
	@Consumes(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Produces(GCatConstants.APPLICATION_JSON_CHARSET_UTF_8)
	@Override
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public String patch(@PathParam(GROUP_ID_PARAMETER) String id, String json) {
		return super.patch(id, json);
	}
	
	@DELETE
	@Path("/{" + GROUP_ID_PARAMETER + "}")
	@Override
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public Response delete(@PathParam(GROUP_ID_PARAMETER) String id,
			@QueryParam(GCatConstants.PURGE_QUERY_PARAMETER) @DefaultValue("false") Boolean purge) {
		return super.delete(id, purge);
	}
	
	@PURGE
	@Path("/{" + GROUP_ID_PARAMETER + "}")
	@Override
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public Response purge(@PathParam(GROUP_ID_PARAMETER) String id) {
		return delete(id, true);
	}
	
	@Override
//	@AuthorizationControl(allowedRoles={Role.CATALOGUE_MANAGER}, exception=NotAuthorizedException.class)
	public Response delete(String name, boolean purge) {
		return delete(name, new Boolean(purge));
	}
	
}
