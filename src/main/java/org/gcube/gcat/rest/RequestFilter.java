package org.gcube.gcat.rest;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;

import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.GCubeSecret;
import org.gcube.common.authorization.utils.secret.JWTSecret;
import org.gcube.common.authorization.utils.secret.Secret;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Provider
@PreMatching
public class RequestFilter implements ContainerRequestFilter, ContainerResponseFilter {
	
	private final static Logger logger = LoggerFactory.getLogger(RequestFilter.class);
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		logger.trace("PreMatching RequestFilter");
		
		SecretManagerProvider.instance.remove();
		SecretManager secretManager = new SecretManager();
		
		String token = AccessTokenProvider.instance.get();
		if(token!=null) {
			Secret secret = new JWTSecret(token);
			secretManager.addSecret(secret);
		}
		
		token = SecurityTokenProvider.instance.get();
		if(token!=null) {
			Secret secret = new GCubeSecret(token);
			secretManager.addSecret(secret);
		}
		
		SecretManagerProvider.instance.set(secretManager);
	}

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		logger.trace("ResponseFilter");
		SecretManagerProvider.instance.remove();
	}

}
