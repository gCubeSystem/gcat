package org.gcube.gcat.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.datacatalogue.metadatadiscovery.bean.jaxb.NamespaceCategory;
import org.gcube.gcat.profile.MetadataUtility;

import com.webcohesion.enunciate.metadata.rs.ResourceGroup;
import com.webcohesion.enunciate.metadata.rs.ResourceLabel;

/**
 * A namespace defines a logical grouping for metadata contained in items.
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Path(Namespace.NAMESPACES)
@ResourceGroup("Item Related APIs")
@ResourceLabel("Namespace APIs")
public class Namespace extends BaseREST implements org.gcube.gcat.api.interfaces.Namespace {
	
	/**
	 * @return the json array containing the licenses available in the catalogue
	 * 
	 * @pathExample /namespaces
	 * @responseExample application/json classpath:/api-docs-examples/namespace/list-namespace-response.json
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String list() {
		setCalledMethod("GET /" + NAMESPACES);
		
		ObjectMapper mapper = new ObjectMapper();
		ArrayNode arrayNode = mapper.createArrayNode();
		
		try {
			List<NamespaceCategory> namespaces = (new MetadataUtility()).getNamespaceCategories();
			for(NamespaceCategory namespaceCategory : namespaces) {
				ObjectNode namespace = mapper.createObjectNode();
				namespace.put("id", namespaceCategory.getId());
				namespace.put("title", namespaceCategory.getTitle());
				namespace.put("name", namespaceCategory.getNamespaceCategoryQName());
				namespace.put("description", namespaceCategory.getDescription());
				arrayNode.add(namespace);
			}
			return mapper.writeValueAsString(arrayNode);
		} catch(Exception e) {
			throw new InternalServerErrorException(e.getMessage());
		}
	}
	
}
