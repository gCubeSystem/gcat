package org.gcube.gcat;

import javax.ws.rs.ApplicationPath;

import org.gcube.gcat.rest.Item;
import org.gcube.gcat.rest.administration.Configuration;
import org.gcube.smartgears.annotations.ManagedBy;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@ApplicationPath("/")
@ManagedBy(GCatInitializator.class)
public class ResourceInitializer extends ResourceConfig {
	
	public ResourceInitializer() {
		packages(Item.class.getPackage().toString());
		packages(Configuration.class.getPackage().toString());
	}
	
}
