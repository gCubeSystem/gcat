package org.gcube.gcat.profile;

import java.io.File;
import java.io.StringReader;
import java.net.URL;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.gcat.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class ProfileTest extends ContextTest {
	
	private static Logger logger = LoggerFactory.getLogger(ProfileTest.class);
	
	@Test
	public void count() throws Exception {
		ISProfile profile = new ISProfile();
		int count = profile.count();
		logger.debug("We have {} types", count);
	}
	
	@Test
	public void list() throws Exception {
		ISProfile profile = new ISProfile();
		ArrayNode arrayNode = profile.list();
		logger.debug("{}", arrayNode);
	}
	
	@Test
	public void read() throws Exception {
		String profileID = "EmptyProfile";
		ISProfile profile = new ISProfile();
		boolean xml = true;
		String ret = profile.read(profileID, xml);
		logger.debug("XML :\n{}", ret);
		xml = false;
		ret = profile.read(profileID, xml);
		logger.debug("JSON : \n{}", ret);
	}
	
	@Test
	public void listRead() throws Exception {
		ISProfile profile = new ISProfile();
		ArrayNode arrayNode = profile.list();
		logger.debug("Found {} profiles", arrayNode.size());
		Iterator<JsonNode> iterator = arrayNode.iterator();
		while(iterator.hasNext()) {
			String profileID = iterator.next().asText();
			boolean xml = true;
			String ret = profile.read(profileID, xml);
			logger.debug("XML :\n{}", ret);
			xml = false;
			ret = profile.read(profileID, xml);
			logger.debug("JSON : \n{}", ret);
		}
	}
	
	@Test
	public void testCreateOrUpdate() throws Exception {
		String xml = "<metadataformat type=\"TestProfile\"><metadatafield><fieldName>test</fieldName><mandatory>false</mandatory><dataType>String</dataType><maxOccurs>1</maxOccurs><note>Test Field</note></metadatafield><metadatafield><fieldName>Population</fieldName><mandatory>false</mandatory><dataType>String</dataType><maxOccurs>*</maxOccurs><defaultValue/><note>The population of the model</note><tagging create=\"true\" separator=\"_\">onValue</tagging><grouping create=\"true\">onFieldName_onValue</grouping></metadatafield></metadataformat>";
		ISProfile profile = new ISProfile();
		profile.createOrUpdate("TestProfile", xml);
	}
	
	@Test
	public void testDelete() throws Exception {
		ISProfile profile = new ISProfile();
		profile.delete("TestProfile");
	}
	
	public File getResourcesDirectory() throws Exception {
		URL logbackFileURL = this.getClass().getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return resourcesDirectory;
	}
	
	public File getProfilesDirectory() throws Exception {
		File resourcesDirectory = getResourcesDirectory();
		return new File(resourcesDirectory, "profile_examples");
	}
	
	public File getSchemaDirectory() throws Exception {
		File resourcesDirectory = getResourcesDirectory();
		return new File(resourcesDirectory, "profile_schemas");
	}
	
	public static String PROFILE_NAME_EXAMPLE = "EmptyProfile";
	public static String PROFILE_EXAMPLE_FILENAME =  PROFILE_NAME_EXAMPLE + ".xml";
	
	public String fileToString(File file) throws Exception {
		try (Stream<String> lines = Files.lines(file.toPath())) {
            String content = lines.collect(Collectors.joining(System.lineSeparator()));
            return content;
		}catch (Exception e) {
			throw e;
		}
	}
	
	@Test
	public void testCreateUpdateDeleteGenericResource() throws Exception {
		File profilesDirectory = getProfilesDirectory();
		File profileFile = new File(profilesDirectory, PROFILE_EXAMPLE_FILENAME);
		String xml = fileToString(profileFile);
		
		ISProfile profile = new ISProfile();
		profile.createOrUpdate(PROFILE_NAME_EXAMPLE, xml);
		/*
		Thread.sleep(TimeUnit.SECONDS.toMillis(30));
		profile.createOrUpdate(PROFILE_NAME_EXAMPLE, "<metadataformat type=\"" + PROFILE_NAME_EXAMPLE + "\" />");
		Thread.sleep(TimeUnit.SECONDS.toMillis(30));
		profile.delete(PROFILE_NAME_EXAMPLE);
		*/
	}
	
	
	protected File getSchemaFile() throws Exception {
		File schemaDirectory = getSchemaDirectory();
		return new File(schemaDirectory, "profile5.xsd");
	}
	
	@Test
	public void validateAgainstXSD() throws Exception {
		File profilesDirectory = getProfilesDirectory();
		File schemaFile = getSchemaFile();
		
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = factory.newSchema(schemaFile.toURI().toURL());
		Validator validator = schema.newValidator();
		
		for(File profileFile : profilesDirectory.listFiles()) {
		
			String xmlString = fileToString(profileFile);
			
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = db.parse( new InputSource(new StringReader(xmlString))); 
			DOMSource xml = new DOMSource(doc);
			
			try {
				validator.validate(xml);
				logger.info("File {} has been successfuly validated against schema.", profileFile.getName());
			}catch (Exception e) {
				logger.error("Error while validating file {}", profileFile.getName());
				throw e;
			}
		}
	}
	
	@Test
	public void testRegex() throws Exception {
		File resDirectory = getResourcesDirectory();
		File regexFile = new File(resDirectory, "regex.txt");
		String regex = fileToString(regexFile);
		Pattern.compile(regex);
	}
	
}
