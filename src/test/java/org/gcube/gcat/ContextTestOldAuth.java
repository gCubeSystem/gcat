/**
 * 
 */
package org.gcube.gcat;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.gcube.common.authorization.utils.manager.SecretManager;
import org.gcube.common.authorization.utils.manager.SecretManagerProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.authorization.utils.secret.SecretUtility;
import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextTestOldAuth {
	
	protected static Properties properties;
	protected static final String PROPERTIES_FILENAME = "token.properties";
	
	public static final String ROOT;
	public static final String VO;
	public static final String VRE;
	
	static {
		properties = new Properties();
		InputStream input = ContextTestOldAuth.class.getClassLoader().getResourceAsStream(PROPERTIES_FILENAME);
		
		try {
			// load the properties file
			properties.load(input);
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
		
		// DEFAULT_TEST_SCOPE_NAME = "/pred4s/preprod/preVRE";
		// DEFAULT_TEST_SCOPE_NAME = "/gcube/devsec/devVRE";
		
		ROOT = "/gcube";
		VO = ROOT + "/devsec";
		VRE = VO + "/devVRE";
		// VO = ROOT + "/devNext";
		// VRE = VO + "/NextNext";
		
	}
	
	public static void set(Secret secret) throws Exception {
		SecretManagerProvider.instance.reset();
		SecretManager secretManager = new SecretManager(); 
		SecretManagerProvider.instance.set(secretManager);
		secretManager.addSecret(secret);
		secretManager.set();
	}
	
	public static void setContext(String token) throws Exception {
		Secret secret = getSecret(token);
		set(secret);
	}

	public static void setContextByName(String fullContextName) throws Exception {
		Secret secret = getSecretByContextName(fullContextName);
		set(secret);
	}
	
	private static Secret getSecret(String token) throws Exception {
		Secret secret = SecretUtility.getSecretByTokenString(token);
		return secret;
	}
	
	private static Secret getSecretByContextName(String fullContextName) throws Exception {
		String token = ContextTestOldAuth.properties.getProperty(fullContextName);
		return getSecret(token);
	}
	
	@BeforeClass
	public static void beforeClass() throws Exception {
		setContextByName(VRE);
	}
	
	@AfterClass
	public static void afterClass() throws Exception {
		SecretManagerProvider.instance.reset();
	}
	
}
