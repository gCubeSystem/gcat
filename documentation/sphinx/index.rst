***********************************************************
Welcome to gCube Catalogue Service (aka gCat) documentation
***********************************************************

gCat is a RESTful application that exposes operations via REST-API.

See the available `REST-API docs <../api-docs/index.html>`_.

Base URL
========

In the production environment, its current value is https://api.d4science.org/gcat


Authorization
=============

D4Science adopts state-of-the-art industry standards for authentication and authorization. 
Specifically, the implementation fully adopts `OIDC (OpenID Connect) <https://openid.net/connect>`_ for authentication and UMA 2 (User-Managed Authorization) for authorization flows. 
`JSON Web Token (JWT) Access token <https://jwt.io/>`_ are used for both authentication and authorization.

Obtain your Bearer token here: https://dev.d4science.org/how-to-access-resources

Service
=======

You can call the methods of the Web Service by writing your REST client application or using existing REST client plugins.


HTTP Statuses
-------------

Any successful operation returns a *200 OK* HTTP status code.
The create operation returns *201 Created*. 
Any Background operation returns *202 Accepted*.
Any operation that does not provide any content returns *204 No Content*.



The most common error statuses a client can obtain are:

* **400 Bad Request** used to indicate a clients error `<https://tools.ietf.org/html/rfc7231#section-6.5.1>`_;
* **401 Unauthorized** used to indicate that the client does not provide the authorization token in the HTTP Header or the client does not have enough right to perform such request `<https://tools.ietf.org/html/rfc7235#section-3.1>`_;
* **404 Not Found** used to indicate that the requested instance does not exist `<https://tools.ietf.org/html/rfc7231#section-6.5.4>`_;
* **405 Method Not Allowed** the used HTTP method is not supported for the requested URL `<https://tools.ietf.org/html/rfc7231#section-6.5.5>`_.
  The response contains the *Allow* HTTP Header indicating the supported HTTP method for such URL `<https://tools.ietf.org/html/rfc7231#section-7.4.1>`_;
* **409 Conflict**  the request could not be completed due to a conflict with the current state of the target resource (e.g. the name of the resource already exists) `<https://tools.ietf.org/html/rfc7231#section-6.5.8>`_;
* **500 Internal Server Error** indicate a server failure `<https://tools.ietf.org/html/rfc7231#section-6.6.1>`_.

You can find a complete list of HTTP Status at `<https://httpstatuses.com/>`_

If you get a *500 Internal Server Error*, please report it in the `gCube ticketing system <https://support.d4science.org>`_. 

Please use this checklist before reporting an error:

* Replicate the request;
* The failure could be temporal due to a network error, a server issue, and many other temporal issues. For this reason, please retry the request after a certain amount of time before reporting the issue;
* indicate how to replicate the error;
* indicate the time when the error occurred (this simplifies identifying the issue).

HTTP Methods
------------

gCat is a pure RESTful service. It uses standard HTTP Methods to perform a listing of collections and CRUD (Create Read Update Delete) operations on instances.


.. table::

	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	|  Operation   | HTTP Method | URL                                    | Success HTTP Status |  Safe  | Idempotent |
	+==============+=============+========================================+=====================+========+============+
	| Supported    | OPTIONS     | /{COLLECTION}                          | 204 No Content      |   Y    |     Y      |
	| HTTP Methods |             |                                        | [#allow]_           |        |            |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| List         | GET         | /{COLLECTION}                          | 200 OK              |   Y    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Count        | GET         | /{COLLECTION}?count=true               | 200 OK              |   Y    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Exists       | HEAD        | /{COLLECTION}                          | 204 No Content      |   Y    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Create       | POST        | /{COLLECTION}                          | 201 Created         |   N    |     N      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Supported    | OPTIONS     | /{COLLECTION}/{INSTANCE_ID}            | 204 No Content      |   Y    |     Y      |
	| HTTP Methods |             |                                        | [#allow]_           |        |            |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Exist        | HEAD        | /{COLLECTION}/{INSTANCE_ID}            | 204 No Content      |   Y    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Read         | GET         | /{COLLECTION}/{INSTANCE_ID}            | 200 OK              |   Y    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Update       | PUT         | /{COLLECTION}/{INSTANCE_ID}            | 200 OK              |   N    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Patch        | PATCH       | /{COLLECTION}/{INSTANCE_ID}            | 200 OK              |   N    |     Y      |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Delete       | DELETE      | /{COLLECTION}/{INSTANCE_ID}            | 204 No Content      |   N    |  N [#del]_ |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+
	| Purge        | DELETE      | /{COLLECTION}/{INSTANCE_ID}?purge=true | 204 No Content      |   N    |  N [#del]_ |
	+              +-------------+----------------------------------------+---------------------+--------+------------+
	|              | PURGE       | /{COLLECTION}/{INSTANCE_ID}            | 204 No Content      |   N    |  N [#del]_ |
	+--------------+-------------+----------------------------------------+---------------------+--------+------------+

.. [#allow] Supported HTTP Methods in **Allow** HTTP Header
	
.. [#del] DELETE has been defined as idempotent.
	
	*Allamaraju* [#Allamaraju]_ argues that DELETE idempotency should be accomplished client-side.
	The server should inform the client if the delete operation succeeded because the resource was deleted or it was not found, i.e., **404 Not Found** error is suggested instead of **204 No Content**.
	The latter situation should be treated as idempotent by the client.
	
	We share the same vision. For this reason, gCat does not provide server-side idempotency for DELETE and PURGE operations.

.. [#Allamaraju] Allamaraju S. RESTful Web Services Cookbook: Solutions for Improving Scalability and Simplicity. O’Reilly. first ed. 2010
 
 
About URL
^^^^^^^^^

The presented URL uses the following convention:

* **{COLLECTION}** is the plural name of the entity type;
* **{INSTANCE_ID}** is an identification that enables univocally identifying the instance in the collection.


About Safety and Idempotency properties
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


* A method is *Safe* if it does not produce any side effects.
  "This does not prevent an implementation from including behaviour that is potentially harmful, that is not entirely read-only, or that causes side effects while invoking a safe method"
  `<https://tools.ietf.org/html/rfc7231#section-4.2.1>`_;
* A method is *Idempotent* if the same operation repeated multiple times has the same side effect as using it one time.
  "repeating the request will have the same intended effect, even if the original request succeeded, though the response might differ"
  `<https://tools.ietf.org/html/rfc7231#section-4.2.2>`_.

You can find more information about HTTP Methods at `<https://restfulapi.net/http-methods/>`_

Uncommon HTTP Methods
^^^^^^^^^^^^^^^^^^^^^

* PATCH method allows to perform a differential update (i.e. an update which provides only the differences and not the whole new representation);
* PURGE method is not a standard but is widely used in services that require this action
  (e.g. `Varnish <https://varnish-cache.org/docs/3.0/tutorial/purging.html>`_, `Squid <https://wiki.squid-cache.org/SquidFaq/OperatingSquid#How_can_I_purge_an_object_from_my_cache.3F>`_).
  gCat provides support for this method, but to support a wider range of clients, it also provides the Purge action via *DELETE* with the additional get parameter ``purge=true``.


Content-Type
------------

Any request must contain an indication of the interesting content type.

The client must specify the **Accept** HTTP Header for any operation returning a result. 

.. code-block:: rest

	Accept: application/json

For any operation sending content to the service, it is necessary to specify the **Content-Type** HTTP Header.

.. code-block:: rest

	Content-Type: application/json

The service accepts and returns only JSON objects.
 
`Profile Collection <../api-docs/resource\_Profile.html>`_ instead can be manipulated in XML only.

Collections
-----------

The following collections are available to any user. 
Catalogue-Editor or above can invoke Non-safe methods only.

* `Item Collection <../api-docs/resource_Item.html>`_;
  
  * `Resource Collection <../api-docs/resource_Resource.html>`_;

* `Profile Collection <../api-docs/resource_Profile.html>`_;
* `Namespace Collection <../api-docs/resource_Namespace.html>`_;
* `License Collection <../api-docs/resource_License.html>`_;
* `Trash Collection <../api-docs/resource_Trash.html>`_;

The following collections are available for Catalogue-Admins or above only:

* `Group Collection <../api-docs/resource_Group.html>`_;
* `Organization Collection <../api-docs/resource_Organization.html>`_;
* `User Collection <../api-docs/resource_User.html>`_;
* `Configuration Collection <../api-docs/resource_Configuration.html>`_.

An overview of the available collections is available at `<../api-docs/index.html>`_;


Roles
-----

Any user has one or more roles in the catalogue.
Only the VRE Manager can assign roles to VRE users. 
 

The catalogue uses the following hierarchic roles:

**Catalogue-Member**: 
	A user with such a role is mainly capable of listing and reading items;
	
**Catalogue-Editor**: 
	A user with such a role is capable of managing the items he/she creates and capable of using other safe APIs;

**Catalogue-Admin**:
	A user with such a role is capable of administrating many aspects of the catalogue;

**Catalogue-Manager**:
	A user with such a role can use all the APIs exposed by the service except item moderation APIs (e.g. approve, reject, ...).


Another role that is not in the role hierarchy:

**Catalogue-Moderator**:
	A user with such a role is capable of invoking the item moderation APIs.


 .. TIP::
    Please note that not all catalogues are moderated.

Moderated Catalogues
====================


Any catalogues can be declared as moderated.
This means that a **Catalogue-Moderator** must approve any submitted items to make them available to the other users of the catalogue.

In a moderated catalogue, an item can be in the following states:

**pending**: 
	The item is published by any allowed author (a Catalogue-Editor or above) but not available to the other users of the catalogue. 
	A Catalogue-Moderator has to approve or reject it;
	
**approved**:
	A Catalogue-Moderator has approved the item published by any allowed users;
	
**rejected**:
	A Catalogue-Moderator has rejected the item published by any allowed users.



The following are the moderation operations that an allowed user can perform on an item. 
To present the moderation operations, we use the following convention:

	``initial_state``  ---**operation** (*User/Role performing the operation*)---> ``final_state``  


``initial_state`` can be ``none``, meaning the item does not exist.


The following are the allowed moderation operations on an item

	``none``  ---**create** (*Author*)---> ``pending``
	
 	``pending`` ---**reject** (*Catalogue-Moderator*)---> ``rejected``
	
	``pending`` ---**approve** (*Catalogue-Moderator*)---> ``approved``
	
	``rejected`` ---**update** (*Author*)---> ``pending``

	``approved`` ---**update** (*Author*)--->  ``pending``


Please check the table below that summarises the item collection operation and the allowed users/roles.

In a moderated catalogue, both the Catalogue-Moderators and the item author can send messages to
discuss the approval process of the item. The messages are related to a specific item.
Any Catalogue-Moderators receive a message sent by an Author.
The author receives a message sent by a Catalogue-Moderator as well as the other Catalogue-Moderators (if any).

Messages can be sent both with an action which changes the status of the item or as an explicit action which does not change the status of the item:

	``pending`` ---**message** (*Author OR Catalogue-Moderator*)---> ``pending``

	``rejected`` ---**message** (*Author OR Catalogue-Moderator*)---> ``rejected``

	``approved`` ---**message** (*Author OR Catalogue-Moderator*)---> ``approved``



The following table summarizes the allowed/forbidden operations depending on the role of the user and the state of the item.
   
.. table::

	+-------------------------------------+-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	| Operation                           | Item State  | Roles                                                                                                          |
	+                                     +             +----------------------+--------------------------+------------------------------------------+-------------------+
	|                                     |             | Catalogue Moderator  | Catalogue Admin/Manager  | Catalogue Editor                         | Catalogue Member  |
	+=====================================+=============+======================+==========================+==========================================+===================+
	| List                                              | Yes all states       | Yes all states           | Yes only approved - All states if Author | Yes only approved |
	+-------------------------------------+-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	| Count                                             | Yes all states       | Yes all states           | Yes only approved - All states if Author | Yes only approved |
	+-------------------------------------+-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	| Create                                            | 403 Forbidden        | Yes -> Pending           | Yes -> Pending                           | 403 Forbidden     |
	+-------------------------------------+-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	| Read                                              | Yes all states       | Yes all states           | Yes only approved - All states if Author | Yes only approved |
	+-------------------------------------+-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	| Update                              | Pending     | Yes -> Pending       | Yes if Author -> Pending | Yes if Author -> Pending                 | 403 Forbidden     |
	+                                     +-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	|                                     | Rejected    | Yes -> Pending       | Yes if Author -> Pending | Yes if Author -> Pending                 | 403 Forbidden     |
	+                                     +-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	|                                     | Approved    | 403 Forbidden        | Yes -> Approved          | Yes if Author -> Pending                 | 403 Forbidden     |
	+-------------------------------------+-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	| Delete/Purge                        | Pending     | Yes                  | Yes                      | Yes if Author                            | 403 Forbidden     |
	+                                     +-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	|                                     | Rejected    | Yes                  | Yes                      | Yes if Author                            | 403 Forbidden     |
	+                                     +-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	|                                     | Approved    | 403 Forbidden        | Yes                      | Yes if Author                            | 403 Forbidden     |
	+-------------------------------------+-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	| Approve a pending item                            | Yes                  | 403 Forbidden            | 403 Forbidden                            | 403 Forbidden     |
	+-------------------------------------+-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	| Reject a pending item                             | Yes                  | 403 Forbidden            | 403 Forbidden                            | 403 Forbidden     |
	+-------------------------------------+-------------+----------------------+--------------------------+------------------------------------------+-------------------+
	| Message about an item                             | Yes                  | Yes if Author            | Yes if Author                            | 403 Forbidden     |
	+-------------------------------------+-------------+----------------------+--------------------------+------------------------------------------+-------------------+

   
The Moderation process has associated notification to authors and Catalogue-Moderators.
Please note that the user who has acted is not self-notified, e.g.
approve operation made by a Catalogue-Moderator notifies the item author and the other Catalogue-Moderators of the VRE.

The following table summarises the addressee of the notification for any action.
 
   
.. table::

	+--------------+----------------------+--------+----------------------+
	| Operation    | Notified user/role                                   |
	+              +----------------------+--------+----------------------+
	|              | Catalogue-Moderators | Author | User made the action |
	+==============+======================+========+======================+
	| Create       | Yes                  | Yes    | Yes (custom message) |
	+--------------+----------------------+--------+----------------------+
	| Update       | Yes                  | Yes    | Yes (custom message) |
	+--------------+----------------------+--------+----------------------+
	| Approve      | Yes + Social Post if          | No                   |
	|              | requested (social_post=true)  |                      |
	|              | and enabled for the VRE       |                      |
	+--------------+----------------------+--------+----------------------+
	| Reject       | Yes                  | Yes    | No                   |
	+--------------+----------------------+--------+----------------------+
	| Delete/Purge | Yes                  | Yes    | No                   |
	+--------------+----------------------+--------+----------------------+
	| Message      | Yes                  | Yes    | No                   |
	+--------------+----------------------+--------+----------------------+



Java Client
===========

We provide the following Java Client out-of-the-box.

 .. TIP::
    If you're coding in Java, it is recommended that you use this Java Client.

**Maven Coordinates**

.. code:: xml

 <groupId>org.gcube.data-catalogue</groupId>
 <artifactId>gcat-client</artifactId>
 <version>[2.2.0, 3.0.0-SNAPSHOT)</version>
 
**Methods Result**

The service exposes `its methods <../api-docs/index.html>`_ using a standard naming approach. Moreover, they accept (in the case of HTTP POST/PUT methods) JSON objects. 

 .. IMPORTANT::
   The result of all methods is always a JSON object as per below:

.. code:: javascript

	{
	    "rating": 0.0,
	    "license_title": "Creative Commons Attribution Share-Alike 4.0",
	    "maintainer": "Frosini Luca",
	    "relationships_as_object": [],
	    "private": false,
	    "maintainer_email": "luca.frosini@isti.cnr.it",
	    "num_tags": 1,
	    "id": "17051d86-c127-4928-9296-d3d7590161fe",
	    "metadata_created": "2022-10-17T12:45:53.118318",
	    "metadata_modified": "2022-10-18T10:30:03.362756",
	    "author": "Frosini Luca",
	    "author_email": "luca.frosini@isti.cnr.it",
	    "state": "active",
	    "version": null,
	    "creator_user_id": "f1b0265c-9983-4f97-a7b6-be3cc0544b27",
	    "type": "dataset",
	    "resources": [],
	    "num_resources": 0,
	    "tags": [
	        {
	            "vocabulary_id": null,
	            "state": "active",
	            "display_name": "Test",
	            "id": "fec9de86-51a2-41b0-aef4-ba06eb39e16d",
	            "name": "Test"
	        }
	    ],
	    "groups": [],
	    "license_id": "CC-BY-SA-4.0",
	    "relationships_as_subject": [],
	    "organization": {
	        "description": "",
	        "created": "2016-05-30T11:30:41.710079",
	        "title": "devVRE",
	        "name": "devvre",
	        "is_organization": true,
	        "state": "active",
	        "image_url": "",
	        "revision_id": "a7eee485-a6d5-4a7b-8f73-b0ed999d5b03",
	        "type": "organization",
	        "id": "3571cca5-b0ae-4dc6-b791-434a8e062ce5",
	        "approval_status": "approved"
	    },
	    "name": "my_test_item_devvre",
	    "isopen": true,
	    "url": "http://www.d4science.org",
	    "notes": "A test item of Luca Frosini",
	    "extras": [
	        {
	            "key": "Item URL",
	            "value": "https://data.dev.d4science.org/ctlg/devVRE/my_test_item_devvre"
	        },
	        {
	            "key": "Language",
	            "value": "EN"
	        },
	        {
	            "key": "system:cm_item_status",
	            "value": "approved"
	        },
	        {
	            "key": "system:cm_item_visibility",
	            "value": "public"
	        },
	        {
	            "key": "system:type",
	            "value": "EmptyProfile"
	        }
	    ],
	    "license_url": "https://creativecommons.org/licenses/by-sa/4.0/",
	    "ratings_count": 0,
	    "title": "My Test Item",
	    "revision_id": "bc0d1f2a-4e97-4810-b951-8b72e8279719"
	}

*Inputs are automatically validated before the request is served.*

      
**Usage examples**

- Example 1
      
.. code:: java

 import org.gcube.gcat.client.Item;

 // count item number
 Item item = new Item();
 int count = item.count();
 ...



Service Discovery on IS
=======================

The service can be discovered in the IS as EService with the following JSON query:

.. code:: json

	{
		"@class": "EService",
		"consistsOf": [
			{
				"@class": "IsIdentifiedBy",
				"target": {
					"@class": "SoftwareFacet",
					"group": "org.gcube.data-catalogue",
					"name": "gcat"
				}
			}
		]
	}

	
	
Service Maven Coordinates
=========================
	
The maven coordinates of the gCat service are:

.. code:: xml

	<groupId>org.gcube.data-catalogue</groupId>
	<artifactId>gcat</artifactId>
	

Postman Collection
=======================

Here you can download the `postman collection to interact with the gCat service <../api-docs/postman/gcat.postman_collection.json>`_.

	